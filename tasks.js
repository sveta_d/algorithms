/* Задан массив  целых чисел. Определить максимальную длину (+ позиции начала и конца)последовательности идущих
подряд различных элементов.*/
function arrayOfNumbers (array){
    var result = array.map(compare);

    function compare (elem, number, array) {
        var arrayResult = [elem];
        for (var i = number + 1; i < array.length; i++) {
          for (var j = 0; j < arrayResult.length; j++) {
              if (array[i] == arrayResult[j]) {
                  return [arrayResult, {'start': number, 'end': number + arrayResult.length - 1}];
              }
          }
          arrayResult.push(array[i]);
      }
      return [arrayResult, {'start': number, 'end': number + arrayResult.length - 1}];
    }

    result.sort(function(a,b){
        return b[0].length > a[0].length;
    });

    for(var k = 0; k < result.length; k++){
        if(result[k][0].length != result[k+1][0].length) break;
    }

    return result.slice(0,k+1);
}
//arrayOfNumbers([2,8,9,3,1,9]);

/*Есть строка, состоящая из слов. Необходимо реализовать наиболее эффективный алгоритм перестановки слов в обратном
порядке в предложении (без выделения дополнительной памяти)*/
function reverseString (string){
    return string.split(' ').reverse().join();
}
//reverseString('I want you read more');

/*Оптимальная расстановка скобок. В арифметическом 	выражении, операндами которого являются целые числа от 0 до 9, а
операциями – бинарные операции «+» и «*», необходимо расставить скобки так, чтобы результат оказался максимальным из всех возможных.*/
/*For one pair of brackets & type of parameter is String */
function optimalBrackets (expression){
    var result = [];

    for(var i = 0; i < expression.length-2; i+=2){
        for(var j = i+3; j <= expression.length; j+=2){
            if(i == 0){
                result.push('(' + expression.slice(0,j) + ')' + expression.slice(j));
            }else{
                result.push(expression.slice(0,i) + '(' +expression.slice(i,j) + ')' + expression.slice(j));
            }
        }
    }
    result = result.map(function(item){ return [eval(item), item]}); // eval() is a build-in function in js;
    return result.sort()[result.length-1];
}
//optimalBrackets('2+8*2');

/*В сундуке у пирата имеется N монет. На следующий год пират взял из сундука M монет. В каждый следующий год пират
добавлял в сундук столько же монет, сколько было у него 2 года назад. Известно, что в X-й год в сундуке у пирата было
Y монет. Требуется определить сколько монет было в сундуке у пирата изначально, и сколько монет взял пират во 2-й год*/
function dowerChest (yearNumber, coins){
   var firstSum,
       secondSum = 0;

    function fibonacci (year) {
        return year <= 1 ? year : fibonacci(year - 1) + fibonacci(year - 2);
    }

    do{
        secondSum++;
        firstSum = coins - fibonacci(yearNumber-1)*secondSum;
        firstSum /= fibonacci(yearNumber);
   }while((firstSum ^ 0) != firstSum); //do while firstSum woun't be the integer

    if(firstSum > secondSum){
        return {'initial amount' : firstSum, 'take off from dower chest in the second year': secondSum};
    }
}
//dowerChest(6, 102);

/*От заданного 	прямоугольника каждый раз отрезается квадрат максимальной площади (длины сторон фигур выражаются
натуральными числами). Найдите число таких квадратов*/
function squaresNumber (oneSide, anotherSide){
    var firstSide = oneSide,
        secondSide = anotherSide,
        squaresCount = 0;

    while(firstSide > 0 && secondSide > 0){
        if(firstSide > secondSide){
            firstSide -=  secondSide;
        }else{
            secondSide -= firstSide;
        }
        squaresCount++;
    }
    return squaresCount;
}
//squaresNumber(12.5, 3);

/*Вычислить n-й член последовательности натуральных чисел: 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, … по заданному N.*/
function findNElement(number){
    var i = 1, count = 0;
    while(count <= number && number != 0){
        i++;
        if(i != 1)count += i;
    }
    return i;
}
//findNElement(11);

/*Реализовать алгоритм ручного деления «в столбик» двух заданных натуральных чисел*/
function dividingNumbers (first, second){
   var result = '',
       dividend = first,
       firstZero;
   do{
       divide(dividend, second);
   }while( first/second != result);

   firstZero = true;
   function divide(first, second){
      if(first < second){
         if(!result){
            result += '0.';
         }else if(result.indexOf('.') == -1){
            result += '.';
         }else{
            firstZero ? firstZero = false : result += '0';
         }
         dividend *= 10;
      }else if(first == second){
         result += 1;
         firstZero = true;
      }else{
         result += parseInt(first/second);
         dividend = (first%second);
         firstZero = true;
      }
   }
   return result;
}
//dividingNumbers(2,5);